package pwValidator;

import java.util.regex.Pattern;


/**
 *
 * @author akret
 */
public class PasswordValidator {
     private static final String regDigit = "^(?=.*[0-9])";
    private static final String regCapital = "^(?=.*[A-Z])";
    private static final String regSpecChar = "(?=.*[@#$%^&+=])";
    private static final String regMinLength = "^.{8}$";
    
    private static final Pattern p1 = Pattern.compile(regDigit);
    private static final Pattern p2 = Pattern.compile(regCapital);
    private static final Pattern p3 = Pattern.compile(regSpecChar);
    private static final Pattern p4 = Pattern.compile(regMinLength);
    
    
    
    private static boolean checkDigit(String password){
        return p1.matcher(password).find();
    }
    private static boolean checkCapital(String password){
        return p2.matcher(password).find();
    }
    private static boolean checkSpecChar(String password){
        return p3.matcher(password).find();
    }
    private static boolean checkMinLength(String password){
        return p4.matcher(password).find();
    }
    public static boolean  isValidPassword(String userInput){        
        return checkDigit(userInput) && 
               checkCapital(userInput) &&
               checkSpecChar(userInput)&&
               checkMinLength(userInput);
        //&& stops when it finds first false
       // & checks all
    }
   
}

