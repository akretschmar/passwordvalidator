
package pwValidator;

import static pwValidator.PasswordValidator.isValidPassword;

/**
 *
 * @author akret
 */
public class pwValidatorMain {

    
    public static void main(String[] args) {
      
             //Some passwords for testing
         //output should be: TRUE FALSE TRUE FALSE
         
        String pw1 = "Ash@pass20"; //passes all requirements
        System.out.println(isValidPassword(pw1));
  
        
        String pw2 = "Ashpass"; //invalid, no special character, no number, <8 characters
        System.out.println(isValidPassword(pw2));
  
       
        String pw3 = "Ash@pas2"; //Boundary in passes, but with minimum characters and reqs
        System.out.println(isValidPassword(pw3));
  
       
        String pw4 = "Ashpass20"; //Boundary out, close but invalid because no special character
        System.out.println(isValidPassword(pw4));
  

  }
    }
    

