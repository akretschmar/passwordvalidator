
package pwValidator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author akret
 */
public class PasswordValidatorTest {
    
    public PasswordValidatorTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
   

    /**
     * Test of isValidPassword method, of class PasswordValidator.
     */
    @Test
    public void testIsValidPasswordDigitRegular() {
        System.out.println("isValidPassword");
        String userInput = "abc0d2Ak";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
        
    }
    @Test
    public void testIsValidPasswordDigitException() {
        System.out.println("isValidPassword");
        String userInput = "abcyddAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
        
    }
      @Test
    public void testIsValidPasswordDigitBoundaryIn() {
        System.out.println("isValidPassword");
        String userInput = "abc0ddAk&";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
        
    }
      @Test
    public void testIsValidPasswordDigitBoundaryOut() {
        System.out.println("isValidPassword");
        String userInput = "abcddAk&ss";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
        
    }
    
      @Test
    public void testIsValidPasswordCapitalRegular() {
        System.out.println("isValidPassword");
        String userInput = "ABCDEFGH";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
        
    }
      @Test
    public void testIsValidPasswordCapitalException() {
        System.out.println("isValidPassword");
        String userInput = "abcdefgh";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
        
    }
      @Test
    public void testIsValidPasswordCapitalBoundaryIn() {
        System.out.println("isValidPassword");
        String userInput = "Abcdefgh";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
        
    }
      @Test
    public void testIsValidPasswordCapitalBoundaryOut() {
        System.out.println("isValidPassword");
        String userInput = "abcdefgh";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
        
    }
      @Test
    public void testIsValidPasswordSpecCharRegular() {
        System.out.println("isValidPassword");
        String userInput = "ab@@@@@@";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
     @Test
    public void testIsValidPasswordSpecCharException() {
        System.out.println("isValidPassword");
        String userInput = "abcdefgh";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
     @Test
    public void testIsValidPasswordSpecCharBoundaryIn() {
        System.out.println("isValidPassword");
        String userInput = "abcdefgh@";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
     @Test
    public void testIsValidPasswordSpecBoundaryOut() {
        System.out.println("isValidPassword");
        String userInput = "abcdefgh";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    @Test
    public void testIsValidPasswordMinLengthRegular() {
        System.out.println("isValidPassword");
        String userInput = "abcdefgh";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
     @Test
    public void testIsValidPasswordMinLengthException() {
        System.out.println("isValidPassword");
        String userInput = "ab";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
     @Test
    public void testIsValidPasswordMinLengthBoundaryIn() {
        System.out.println("isValidPassword");
        String userInput = "abcdefgh";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
     @Test
    public void testIsValidPasswordMinLengthBoundaryOut() {
        System.out.println("isValidPassword");
        String userInput = "abcdefg";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userInput);
        assertEquals(expResult, result);
    }
    
}
